import formatFeedData from './formatFeedData'

const retrieveCalendars = (icsFeeds) => {
  return Promise.all(icsFeeds.map(async (source, i) => {
    const icalFeed = await fetch(source.url).then(r => r.text())

    return formatFeedData(source, i, icalFeed, source.url)
  }))
}

export default retrieveCalendars

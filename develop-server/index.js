
const path = require('path')

const cors = require('cors')
const express = require('express')
const fetch = require('isomorphic-unfetch')

const calendarSources = require('./data/calendars')

const app = express()

app.use(cors())
app.options('*', cors())

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../client/dist/index.html'))
})

app.get('/api/calendars', async (req, res) => {
  res.json(calendarSources)
})

app.get('/api/calendars/proxy', async (req, res) => {
  const { url } = req.query

  if (!url) {
    res.status(400).send('bad request, url query required')
  }

  try {
    const proxyResponse = await fetch(url).then(r => r.text())
    res.send(proxyResponse)
  } catch (error) {
    res.status(400).send('error retrieving feed', error.message)
  }
})

app.use(express.static(path.join(__dirname, '../client/dist')))

const port = process.env.PORT || 3000
app.listen(port, () => {
  console.log(`development server for js-calendar-from-ics is UP on PORT ${port}`)
})

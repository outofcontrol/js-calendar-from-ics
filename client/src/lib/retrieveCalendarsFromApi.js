import formatFeedData from './formatFeedData'

const retrieveCalendars = async (apiUrl) => {
  const calendarData = await fetch(apiUrl).then(r => r.json())

  return await Promise.all(calendarData.map(async (data, i) => {
    const feedUrl = `${apiUrl}/proxy?url=${encodeURIComponent(data.url)}`
    const icalFeed = await fetch(feedUrl).then(r => r.text())

    return formatFeedData(data, i, icalFeed, feedUrl)
  }))
}

export default retrieveCalendars

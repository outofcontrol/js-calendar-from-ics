# js-calendar-from-ics | Development Server

### The What
This folder houses a small express app intended for use as a host server when running the js-calendar-from-ics app in development.

When attempting to retrieve ICS feeds directly from the browser, the built-in browser security will block those requests due to CORS. The workaround is to start the browser with certain development flags to disable this.

As an alternative, use this server when developing to retrieve the ICS feeds which can then be sent to the browser without issue.

### Getting started
The following programs are required:
- `Node.js`
- `npm` or `yarn` (examples will assume the use of `yarn` but they are interchangeable)

You'll want at least one ICS feed to make it useful. The app will pull information to load the ICS feeds from the `data/calendars.json` file. This will be ignored in .git so as to keep your information private, so you'll need to create this file yourself. An example of the file and what data is in it is located at `data/calendars.example.json`.

Once you have the programs installed and the `data/calendars.json` file created, you can start the app. Run the following commands
- `yarn install` (this will install all the packages required to run the server)
- `yarn start` (this starts the server)

The server will start on port `3000`, so will be accessible locally as `http:\\localhost:3000`. This will serve the js-calendar-from-ics app.

To retrieve the calendars from the feeds you have provided in the `data/calendars.json` file, send a GET request to `http:\\localhost:3000\api\calendars`.
// Leave this variable blank if retrieving sources from a server
// Otherwise place the sources in this variable and ensure browser CORS is disabled
// "show_details" is optional. If it is not provided, it will be considered false. If false, the user cannot click on an event to view more details.
window.icsFeeds = []
// Example ics source data object
// [{
//   url: 'web-link-to-resource',
//   title: 'Name Used to Identify this Calendar',
//   event_properties: {
//     color: "A valid CSS color used to distinguish the different calendar sources" ,
//   },
//    "show_details": true
// }, {
//  ...another resource with same format...
// }]

// ----------------------------------------------

// default api url used if not entered is http://localhost:3000/api/calendars
window.calendarApiUrl = ''

// Title to display above the calendar.
// If nothing is entered it will be 'My Calendar - Our Selected Event Feeds'
window.calendarTitle = ''

// Title of the window
// If nothing is entered it will be 'Our Events'
window.windowTitle = ''

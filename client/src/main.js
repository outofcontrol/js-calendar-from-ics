import Vue from 'vue'
import App from './App.vue'

import 'bootswatch/dist/flatly/bootstrap.min.css'
import '@fortawesome/fontawesome-free/css/all.min.css'

Vue.config.productionTip = false

document.title = window.windowTitle || 'Our Events'

new Vue({
  render: h => h(App),
}).$mount('#app')

import ICAL from 'ical.js'

const formatIcalFeedData = (calendarData, calendarIndex, rawIcalData, feedUrl = '') => {
  const retval = {
    id: `${calendarData.url}-${calendarIndex}`,
    url: feedUrl,
    title: calendarData.title,
    color: calendarData.event_properties.color,
    hidden: false,
    format: 'ics',
    showDetails: !!(calendarData.show_details || false),
  }

  const jCalData = new ICAL.Component(ICAL.parse(rawIcalData))

  const tzData = jCalData.getFirstSubcomponent('vtimezone')
  if (tzData) {
    try {
      retval.tzid = tzData.getFirstPropertyValue('tzid')
      retval.tzoffset = tzData.getFirstSubcomponent('standard')
        .getFirstPropertyValue('tzoffsetto')
        .toString()
    } catch (error) {}
  }

  return retval
}

export default formatIcalFeedData

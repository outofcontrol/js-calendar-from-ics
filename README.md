# JS Calendar from ICS

### The What
Convert iCalendar ICS to [FullCalendar][0] JSON using [ical.js][1].

### Why Use This?
With this application, you can continue to use your own ICS source but combine it together with others for one view. One example of how this could be used is coordination with groups that each have a different teleconference calendar. By inputting all those feeds and viewing it on this calendar, you can easily see what timeslots are available for everybody.
Because

### Getting Started
**Important!**  
Ensure you provide sufficient CORS access on the web server hosting the ICS feed(s) or nothing will display (visible in browser console).

At the moment, you cannot immediately use any files in the client folder without some kind of build setup. If you are comfortable with that and know how to build a Webpack project, you can build the source code yourself from the client folder. Otherwise, the [releases][2] in this repository will contain build files that you can start using right away.

If you are using a pre-build release, the `index.html` file can be loaded in the browser or served via a webserver with no further support needed.

However, you'll want to provide your ICS feeds to get use out of it. The `ics-data.js` file has a spot you can fill out at the top of the file with your feeds. Information is in that file for what the data should look like and where to put it exactly.

### Credits
- All JS libraries used (contained in the `client/lib` folder), authors are marked in the scripts
- The script this is based on, which provides an out-of-the-box working solution to view calendars from ICS URL feeds using FullCalendar: [https://github.com/leonaard/icalendar2fullcalendar](https://github.com/leonaard/icalendar2fullcalendar

---

The original repo for this app can be found at https://gitlab.nomagic.uk/popi/js_calendar_from_ics/

PRs welcome of course, especially to provide compatibility with more recent versions of FullCalendar.io

[0]: http://fullcalendar.io/
[1]: https://mozilla-comm.github.io/ical.js/
[2]: https://gitlab.com/outofcontrol/js-calendar-from-ics/-/releases

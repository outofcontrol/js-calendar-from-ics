/* originally from https://github.com/fullcalendar/fullcalendar */
import { createPlugin, addDays } from '@fullcalendar/common'
import moment from 'moment-timezone'
import { IcalExpander } from './icalExpander'

const eventSourceDef = {
  parseMeta (refined) {
    if (refined.url && refined.format === 'ics') {
      return {
        url: refined.url,
        format: 'ics',
      }
    }
    return null
  },

  fetch (arg, onSuccess, onFailure) {
    const { meta } = arg.eventSource
    let { internalState } = meta

    function handleICalEvents (errorMessage, iCalExpander, xhr) {
      if (errorMessage) {
        onFailure({ message: errorMessage, xhr })
      } else {
        onSuccess({ rawEvents: expandICalEvents(iCalExpander, arg.range, arg), xhr })
      }
    }

    /*
    NOTE: isRefetch is a HACK. we would do the recurring-expanding in a separate plugin hook,
    but we couldn't leverage built-in allDay-guessing, among other things.
    */
    if (!internalState || arg.isRefetch) {
      internalState = meta.internalState = { // our ghetto Promise
        completed: false,
        callbacks: [handleICalEvents],
        errorMessage: '',
        iCalExpander: null,
        xhr: null,
      }

      requestICal(
        meta.url,
        (rawFeed, xhr) => {
          const iCalExpander = new IcalExpander({
            ics: rawFeed,
            skipInvalidDates: true,
          })

          for (const callback of internalState.callbacks) {
            /* eslint-disable-next-line standard/no-callback-literal */
            callback('', iCalExpander, xhr)
          }

          internalState.completed = true
          internalState.callbacks = []
          internalState.iCalExpander = iCalExpander
          internalState.xhr = xhr
        },
        (errorMessage, xhr) => {
          for (const callback of internalState.callbacks) {
            callback(errorMessage, null, xhr)
          }

          internalState.completed = true
          internalState.callbacks = []
          internalState.errorMessage = errorMessage
          internalState.xhr = xhr
        },
      )
    } else if (!internalState.completed) {
      internalState.callbacks.push(handleICalEvents)
    } else {
      handleICalEvents(internalState.errorMessage, internalState.iCalExpander, internalState.xhr)
    }
  },
}

function requestICal (url, successCallback, failureCallback) {
  const xhr = new XMLHttpRequest()
  xhr.open('GET', url, true)
  xhr.onload = () => {
    if (xhr.status >= 200 && xhr.status < 400) {
      successCallback(xhr.responseText, xhr)
    } else {
      failureCallback('Request failed', xhr)
    }
  }
  xhr.onerror = () => failureCallback('Request failed', xhr)
  xhr.send(null)
}

function expandICalEvents (iCalExpander, range, arg) {
  // expand the range. because our `range` is timeZone-agnostic UTC
  // or maybe because ical.js always produces dates in local time? i forget
  const rangeStart = addDays(range.start, -1)
  const rangeEnd = addDays(range.end, 1)

  const defaultTimezone = 'UTC'
  const displayTimezone = arg.context.options.timeZone || 'UTC'

  const iCalRes = iCalExpander.between(rangeStart, rangeEnd) // end inclusive. will give extra results
  const expanded = []

  // TODO: instead of using startDate/endDate.toString to communicate allDay,
  // we can query startDate/endDate.isDate. More efficient to avoid formatting/reparsing.

  // single events
  for (const iCalEvent of iCalRes.events) {
    const start = iCalEvent.startDate.icaltype === 'date'
      ? iCalEvent.startDate.toString()
      : moment.tz(
        iCalEvent.startDate.toString(),
        iCalEvent.startDate.timezone || defaultTimezone,
      ).tz(displayTimezone).format()

    const end = (specifiesEnd(iCalEvent) && iCalEvent.endDate)
      ? iCalEvent.endDate.icaltype === 'date'
        ? iCalEvent.endDate.toString()
        : moment.tz(
          iCalEvent.endDate.toString(),
          iCalEvent.endDate.timezone || defaultTimezone,
        ).tz(displayTimezone).format()
      : null

    expanded.push({
      ...buildNonDateProps(iCalEvent),
      start,
      end,
    })
  }

  // recurring event instances
  for (const iCalOccurence of iCalRes.occurrences) {
    const iCalEvent = iCalOccurence.item

    const start = iCalEvent.startDate.icaltype === 'date'
      ? iCalOccurence.startDate.toString()
      : moment.tz(
        iCalOccurence.startDate.toString(),
        iCalEvent.startDate.timezone || defaultTimezone,
      ).tz(displayTimezone).format()

    const end = (specifiesEnd(iCalEvent) && iCalEvent.endDate)
      ? iCalEvent.endDate.icaltype === 'date'
        ? iCalOccurence.endDate.toString()
        : moment.tz(
          iCalOccurence.endDate.toString(),
          iCalEvent.endDate.timezone || defaultTimezone,
        ).tz(displayTimezone).format()
      : null

    expanded.push({
      ...buildNonDateProps(iCalEvent),
      start,
      end,
    })
  }

  return expanded
}

function buildNonDateProps (iCalEvent) {
  return {
    title: iCalEvent.summary,
    url: extractEventUrl(iCalEvent),
    extendedProps: {
      location: iCalEvent.location,
      organizer: iCalEvent.organizer,
      description: iCalEvent.description,
    },
  }
}

function extractEventUrl (iCalEvent) {
  const urlProp = iCalEvent.component.getFirstProperty('url')
  return urlProp ? urlProp.getFirstValue() : ''
}

function specifiesEnd (iCalEvent) {
  return Boolean(iCalEvent.component.getFirstProperty('dtend')) ||
    Boolean(iCalEvent.component.getFirstProperty('duration'))
}

export default createPlugin({
  eventSourceDefs: [eventSourceDef],
})
